#include <iostream>
#include <string>
#include <cstring>
#include <fstream>

using namespace std;

enum Side { HOME, VISITORS, NONE };

const int MAX_PLAYERS = 100;

class FootballPlayer {
private:
    // TODO
	string name;
	int points;

public:
    FootballPlayer(const string &name);
    const string &getName() const;
    int getPointsScored() const;
    void recordScore(int points);
};

// TODO: FootballPlayer methods

FootballPlayer::FootballPlayer(const string& name)
{
	this->name = name;
	points = 0;
}

const string& FootballPlayer::getName() const
{
	return name;
}

int FootballPlayer::getPointsScored() const
{
	return points;
}

void FootballPlayer::recordScore(int points)
{ this->points += points; }


class FootballTeam {
private:
    // TODO
	FootballPlayer** players;
	string name;
	unsigned size;
public:
    FootballTeam(const string &name);
    ~FootballTeam();
    void addPlayer(const string &name);
    FootballPlayer *getPlayer(const string &name) const;
};

// TODO: FootballTeam methods

FootballTeam::FootballTeam(const string& name)
{
	size = 0;
	players = new FootballPlayer*[size];
	this->name = name;
}

void FootballTeam::addPlayer(const string& name)
{
	if (size == MAX_PLAYERS) return;
	players[size++] = new FootballPlayer(name);
}

FootballPlayer* FootballTeam::getPlayer(const string& name) const
{
	for (unsigned i = 0; i < size; i++)
	{
		if (players[i]->getName() == name)
			return players[i];
	}
	return nullptr;
}

FootballTeam::~FootballTeam()
{
	for (unsigned i = 0; i < size; i++)
	{
		delete players[i];
		players[i] = nullptr;
	}
	delete players;
	players = nullptr;
}

struct FootballTeamStats {
    int score;
    string mvpName;
    int mvpScore;
};

struct FootballGameStats {
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

class FootballGame {
private:
    // TODO
	FootballGameStats gameStats;
	string homeName;
	string visitorsName;
	FootballTeam* homeTeam;
	FootballTeam* visitorTeam;
public:
    FootballGame(const string &homeName, const string &visitorsName);
    ~FootballGame();
    void addPlayer(Side side, const string &name);
    void recordScore(Side side, const string& playerName, int points);
    FootballGameStats getStats() const;
};

// TODO: FootballGame methods
FootballGame::FootballGame(const string& homeName, const string& visitorsName)
{
	this->homeName = homeName;
	this->visitorsName = visitorsName;
	homeTeam = new FootballTeam(homeName);
	visitorTeam = new FootballTeam(visitorsName);
	gameStats.homeStats.score = 0;
	gameStats.visitorsStats.score = 0;
	gameStats.homeStats.mvpScore = 0;
	gameStats.visitorsStats.mvpScore = 0;
}

void FootballGame::addPlayer(Side side, const string &name)
{
	if (side == HOME) homeTeam->addPlayer(name);
	else visitorTeam->addPlayer(name);
}

void FootballGame::recordScore(Side side, const string& playerName, int points)
{
	if (side == HOME)
	{
		gameStats.homeStats.score += points;
		homeTeam->getPlayer(playerName)->recordScore(points);
		cout << "Home: " << playerName << ": " << homeTeam->getPlayer(playerName)->getPointsScored() << endl;
		if (homeTeam->getPlayer(playerName)->getPointsScored() > gameStats.homeStats.mvpScore)
		{
			gameStats.homeStats.mvpName = playerName;
			gameStats.homeStats.mvpScore = homeTeam->getPlayer(playerName)->getPointsScored();
		}
	}
	else if (side == VISITORS)
	{
		gameStats.visitorsStats.score += points;
		visitorTeam->getPlayer(playerName)->recordScore(points);
		cout << "Visitor: " << playerName << ": " << visitorTeam->getPlayer(playerName)->getPointsScored() << endl;
		if (visitorTeam->getPlayer(playerName)->getPointsScored() > gameStats.visitorsStats.mvpScore)
		{
			gameStats.visitorsStats.mvpName = playerName;
			gameStats.visitorsStats.mvpScore = visitorTeam->getPlayer(playerName)->getPointsScored();
		}
	}
	gameStats.winner = gameStats.homeStats.score > gameStats.visitorsStats.score ? HOME : VISITORS;
}

FootballGameStats FootballGame::getStats() const
{ return gameStats; }

FootballGame::~FootballGame()
{
	delete homeTeam;
	delete visitorTeam;
}

class FootballFileParser {
private:
    // TODO
	ifstream file;
	const unsigned maxSize = 50;
	FootballGameStats gameStats;
	char* homeName;
	char* visitorsName;
	int convertPoints(const string& pointsEarned)
	{
		//TD = touchdown (6 points)
		//FG = field goal (3 points)
		//S = safety (2 points)
		//EP = extra point (1 point)

		int points = 0;
		if (pointsEarned == "TD")
			points = 6;
		else if (pointsEarned == "FG")
			points = 3;
		else if (pointsEarned == "S")
			points = 2;
		else if (pointsEarned == "EP")
			points = 1;
		return points;
	}

public:
    FootballFileParser(const string& filepath);
    bool read();  // Returns false if parsing fails
    void printStats() const;
};

// TODO: FootballFileParser methods

FootballFileParser::FootballFileParser(const string& filepath)
{
	file.open(filepath);
	homeName = new char[maxSize];
	visitorsName = new char[maxSize];
	if (file.fail())
	{
		cout << "File Error" << endl;
		exit(EXIT_FAILURE);
	}
}

bool FootballFileParser::read()
{

	//get home and visitor team name
	file.getline(homeName, maxSize);
	file.ignore(4096, '\n');
	file.getline(visitorsName, maxSize);
	file.ignore(4096, '\n');

	FootballGame* game = new FootballGame(homeName, visitorsName);
	const string delim = ",";
	char str[maxSize];
	//read and store inputs
	//file format: SIDE, NAME, SCORE
	while (!file.eof())
	{
		Side side = NONE;
		string name;
		int points = 0;
		size_t pos = 0;
		const int tokenNum = 3;
		file.getline(str, maxSize);
		for (int i = 0; i < tokenNum; i++)
		{
			pos = static_cast<string>(str).find(delim);

			//File format: SIDE, NAME, SCORE
			//Gets token, then removes token from string
			string token = static_cast<string>(str).substr(0, pos);
			string newStr = static_cast<string>(str).substr(pos + 1);
			strcpy(str, newStr.c_str());
			if (token == "H") side = HOME;
 			else if (token == "V") side = VISITORS;
 			else if (token.length() < 3) points = convertPoints(token);
 			else name = token;
		}
		//Check for parse failure
		if (name == "" || points == 0 || side == NONE) return false;
		else
		{
			game->addPlayer(side, name);
			game->recordScore(side, name, points);
		}
	}
	gameStats = game->getStats();
	return true;
}

void FootballFileParser::printStats() const
{

	cout << endl;
	cout << "Home: " << gameStats.homeStats.score << endl <<
			"Visitors: " << gameStats.visitorsStats.score << endl;

	cout << "Winner: ";
	if (gameStats.winner == 0) cout << "Home" << endl;
	else if (gameStats.winner == 1) cout << "Visitors" << endl;

	cout << "Highest Scorers: " << endl <<
			'\t' << gameStats.homeStats.mvpName << " (" << gameStats.homeStats.mvpScore << " points)" << endl <<
			'\t' << gameStats.visitorsStats.mvpName << " (" << gameStats.visitorsStats.mvpScore << " points)" << endl;

}

int main(int argc, char **argv)
{

	string filename;
	if (argc > 1)
		filename = argv[1];
	else
	{
		cout << "Usage: FILENAME" << endl;
		return 1;
	}
	FootballFileParser parser(filename);
	if (parser.read())
		parser.printStats();
	else
		cout << "Reading failure" << endl;

}




